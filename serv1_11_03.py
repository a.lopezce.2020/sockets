#!/usr/bin/python
from http.server import HTTPServer, BaseHTTPRequestHandler

PORT_NUMBER = 21999

class myServidor(BaseHTTPRequestHandler):

        # Este metodo gestiona las peticiones GET de HTTP
        def do_GET(self):
                self.send_response(200)
                self.send_header('Content-type','text/html; charset=utf-8')
                self.end_headers()
                # Nos retorna la respuesta
                self.wfile.write("Hello World !".encode("utf-8"))    #Va a imprimir un hola mundo
                return
#De aquí para abajo son las pruebas
try:
        server = HTTPServer(('', PORT_NUMBER), myServidor)
        print('Started httpserver on port ' , PORT_NUMBER)

        #Wait forever for incoming htto requests
        server.serve_forever()

except KeyboardInterrupt:
        print('Control-C received, shutting down the web server')
        server.socket.close()

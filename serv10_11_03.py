#!/usr/bin/python
from http.server import HTTPServer, BaseHTTPRequestHandler


PORT_NUMBER = 21999

class miServidor(BaseHTTPRequestHandler):

        # Este metodo gestiona las peticiones GET de HTTP                       # Tener el codigo dividido de esta manera es util si quiero utilizar 
                                                                                # un bucle para que me devuelva (mas de una vez si es necesario) una linea
        def do_GET(self):
                self.send_response(200)
                self.send_header('Content-type','text/html')
                self.end_headers()
                # Nos retorna la respuesta
                with open('index.html','r' ) as f:                       #en caso de que queramos escribir ponemos w
                        content = f.readlines()                          #este imprime una lista
                        #content_read = f.read()                         #este imprime un string
                        for line in content:
                                self.wfile.write(line.encode("utf-8"))
                return

try:
        server = HTTPServer(('', PORT_NUMBER), miServidor)
        print("localhost" , PORT_NUMBER)

        #Wait forever for incoming http requests
        server.serve_forever()

except KeyboardInterrupt:
        print('Control-C received, shutting down the web server')
        server.socket.close()
